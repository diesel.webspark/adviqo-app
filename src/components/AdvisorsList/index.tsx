import React, { useEffect, useState } from 'react';
import { List, Skeleton, Divider, Result } from 'antd';
import { ApiService } from '../../services/api';
import InfiniteScroll from 'react-infinite-scroll-component';

import SortingHeader from '../SortingHeader';
import { Sorting } from '../../enums/Sorting';
import { Statuses } from '../../enums/Statuses';
import { ISortState } from '../../interfaces/ISortState';
import { IUser } from '../../interfaces/IUser';
import ListItem from './ListItem';
import { IAdvisorListState } from '../../interfaces/IAdvisorListState';

const initialSortState: ISortState = {
  sorting: Sorting.NONE,
  languages: [],
  status: Statuses.NONE,
  page: 1,
};

const initialState: IAdvisorListState = {
  isLoading: false,
  listItems: [],
  sortState: initialSortState,
  error: null,
  fullListLoaded: false,
};

function AdvisorsList() {
  const [{ sortState, isLoading, listItems, error, fullListLoaded }, setState] =
    useState<IAdvisorListState>(initialState);
  const patchState = (patch: Partial<IAdvisorListState>) =>
    setState((state) => ({
      ...state,
      ...patch,
    }));

  const getAdvisorsList = async () => {
    patchState({ isLoading: true });
    try {
      const { list, isLastPage } = await ApiService.getUsers(sortState);

      const updatedItems = sortState.page === 1 ? list : listItems.concat(list);
      patchState({
        isLoading: false,
        error: null,
        fullListLoaded: isLastPage,
        listItems: updatedItems,
      });
    } catch (error: any) {
      patchState({
        isLoading: false,
        error: error.message,
      });
    }
  };

  useEffect(() => {
    if (isLoading) {
      return;
    }
    getAdvisorsList();
  }, [sortState]);

  function handleChangeSortingState<K extends keyof ISortState>(
    property: K,
    value: ISortState[K]
  ) {
    patchState({
      sortState: {
        ...sortState,
        [property]: value,
        page: 1,
      },
    });
  }

  const onLoadMore = () => {
    patchState({
      sortState: {
        ...sortState,
        page: sortState.page + 1,
      },
    });
  };

  if (error) {
    return <Result status="warning" title={error} />;
  }
  return (
    <>
      <SortingHeader handleChangeSortingState={handleChangeSortingState} />
      <InfiniteScroll
        dataLength={listItems.length}
        next={onLoadMore}
        hasMore={!fullListLoaded}
        loader={<Skeleton avatar paragraph={{ rows: 5 }} active />}
        endMessage={<Divider plain>That&apos;s all advisors</Divider>}
        scrollableTarget="scrollableDiv"
      >
        <List
          className="demo-loadmore-list"
          loading={isLoading}
          itemLayout="horizontal"
          dataSource={listItems}
          renderItem={(item: IUser) => <ListItem user={item} />}
        />
      </InfiniteScroll>
    </>
  );
}

export default AdvisorsList;
