import React from 'react';
import AdvisorsList from '../index';
import { render, cleanup } from '@testing-library/react';
import axios from '../../../__mocks__/axios';

afterEach(cleanup);

it('Async axios request works', async () => {
  axios.get.mockResolvedValue({
    data: {
      isLastPage: false,
      list: [
        {
          id: 1,
          firstName: 'Asa',
          lastName: 'Stokes',
          languages: ['ES', 'EN'],
          reviews: 35,
          status: 'online',
          avatar: 'https://loremflickr.com/200/200/cats?60628',
        },
      ],
    },
  });
  const { findByText, container } = render(<AdvisorsList />);
  expect(await findByText(/Stokes/i)).toBeInTheDocument();
  expect(axios.get).toHaveBeenCalledTimes(1);
});
