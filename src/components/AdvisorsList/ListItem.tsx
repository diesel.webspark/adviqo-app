import React from 'react';
import { IUser } from '../../interfaces/IUser';
import IconText from '../common/IconText';
import {
  BellOutlined,
  FontSizeOutlined,
  MessageOutlined,
} from '@ant-design/icons';
import { Avatar, List } from 'antd';

function AdvisorListItem(props: { user: IUser }) {
  const { user } = props;
  return (
    <List.Item
      actions={[
        <IconText
          icon={MessageOutlined}
          text="Reviews"
          number={user.reviews}
          key="list-vertical-message"
        />,
        <IconText
          icon={FontSizeOutlined}
          text={user.languages.join(', ')}
          key="list-vertical-message"
        />,
        <IconText
          icon={BellOutlined}
          text={user.status}
          key="list-vertical-message"
        />,
      ]}
    >
      <List.Item.Meta
        avatar={<Avatar src={user.avatar} size={64} />}
        title={
          <a href="src/components/AdvisorsList/ListItem#">
            {user.firstName} {user.lastName}
          </a>
        }
        description="Voluptatem repellat consequatur deleniti qui quibusdam harum cumque."
      />
    </List.Item>
  );
}

export default AdvisorListItem;
