import React from 'react';
import styles from './styles.module.css';
import Container from '../common/Container';
import AdvisorsList from '../AdvisorsList';

function Main() {
  return (
    <div className={styles.main}>
      <Container>
        <AdvisorsList />
      </Container>
    </div>
  );
}

export default Main;
