import React from 'react';
import styles from './styles.module.css';
import Container from '../common/Container';
import { FormattedMessage } from 'react-intl';

function Header() {
  return (
    <div className={styles.header}>
      <Container>
        <FormattedMessage id="welcome-title" />
      </Container>
    </div>
  );
}

export default Header;
