import React from 'react';
import { render, screen } from '@testing-library/react';
import Containter from '../index';

test('renders child', () => {
  const { getByText } = render(<Containter>Test</Containter>);
  expect(getByText(/Test/i).textContent).toBe('Test');
});
