import React from 'react';
import { IContentContainer } from '../../../interfaces/IcontentContainer';

function Container(props: IContentContainer) {
  const { children } = props;
  return <div className="container">{children}</div>;
}

export default Container;
