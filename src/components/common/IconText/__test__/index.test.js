import React from 'react';
import { render, screen } from '@testing-library/react';
import IconText from '../index';
import {
  BellOutlined,
} from '@ant-design/icons';

test('renders children', () => {
  const { getByText } = render(
    <IconText icon={BellOutlined} text={'Test'} number={1111} />
  );
  expect(getByText(/Test/i).textContent).toBe('Test');
  expect(getByText(/1111/i).textContent).toBe('1111');
});
