import React from 'react';
import { Space } from 'antd';

function IconText({
  icon,
  text,
  number,
}: {
  icon: React.FC;
  text: string;
  number?: number;
}) {
  return (
    <Space>
      {React.createElement(icon)}
      <b>{number}</b>
      <b>{text}</b>
    </Space>
  );
}

export default IconText;
