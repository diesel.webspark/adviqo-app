import React from 'react';
import { ISortingSelect } from '../../interfaces/ISortingSelect';
import { Select } from 'antd';
import { Languages } from '../../enums/Languages';
import styles from './styles.module.css';

const { Option } = Select;

function SortingSelect(props: ISortingSelect) {
  return (
    <div>
      <div className={styles.selectTitle}>{props.title}</div>
      <Select
        className={styles.select}
        placeholder={props.title}
        {...(props.mode ? { mode: props.mode } : {})}
        onChange={(value) => {
          props.cb(props.type, value);
        }}
      >
        {props.options.map((item) => (
          <Option key={item} value={item}>
            {item || 'None'}
          </Option>
        ))}
      </Select>
    </div>
  );
}

export default SortingSelect;
