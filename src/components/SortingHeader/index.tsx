import React from 'react';
import { Select, Space } from 'antd';
import styles from './styles.module.css';
import { Languages } from '../../enums/Languages';
import { Statuses } from '../../enums/Statuses';
import { Sorting } from '../../enums/Sorting';
import SortingSelect from './SortingSelect';
import { GetUserParams } from '../../enums/GetUserParams';
import { ISortingSelect } from '../../interfaces/ISortingSelect';
import { useIntl } from 'react-intl';

function SortingHeader(props: { handleChangeSortingState: Function }) {
  const intl = useIntl();
  const { handleChangeSortingState } = props;
  const config: Omit<ISortingSelect, 'cb'>[] = [
    {
      title: intl.formatMessage({
        id: 'languages-label',
      }),
      mode: 'multiple',
      type: GetUserParams.languages,
      options: Object.values(Languages),
    },
    {
      title: intl.formatMessage({
        id: 'statuses-label',
      }),
      type: GetUserParams.status,
      options: Object.values(Statuses),
    },
    {
      title: intl.formatMessage({
        id: 'reviews-label',
      }),
      type: GetUserParams.sorting,
      options: Object.values(Sorting),
    },
  ];

  return (
    <div className={styles.sortingHeader}>
      <Space>
        {config.map((item) => (
          <SortingSelect
            key={item.type}
            {...item}
            cb={handleChangeSortingState}
          />
        ))}
      </Space>
    </div>
  );
}

export default SortingHeader;
