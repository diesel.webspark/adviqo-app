import axios, { AxiosResponse } from 'axios';
import { IUser } from '../interfaces/IUser';
import { ISortState } from '../interfaces/ISortState';

export type ProductResponse = {
  list: IUser[];
  isLastPage: boolean;
};

export class ApiService {
  static async getUsers(params: ISortState) {
    return await axios
      .get<ProductResponse>(`${process.env.REACT_APP_API}`, {
        params,
      })
      .then(this.unpackResponse);
  }

  private static unpackResponse<T>({ data }: AxiosResponse<T>) {
    return data;
  }
}
