import React from 'react';
import styles from './syles.module.css';
import Main from './components/Main';
import Header from './components/Header';
import { IntlProvider } from 'react-intl';
import messages from './i18n/en/messages';

function App() {
  return (
    <IntlProvider locale="en" messages={messages}>
      <div data-testid="app" className={styles.App}>
        <Header />
        <Main />
      </div>
    </IntlProvider>
  );
}

export default App;
