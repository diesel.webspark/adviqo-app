const enMessages = {
  'welcome-title': 'Welcome to advisors list App',
  'languages-label': 'Choose languages',
  'statuses-label': 'Filter by status',
  'reviews-label': 'Sort by reviews',
};

export default enMessages;
