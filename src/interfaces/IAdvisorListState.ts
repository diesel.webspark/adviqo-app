import { IUser } from './IUser';
import { ISortState } from './ISortState';

export interface IAdvisorListState {
  isLoading: boolean;
  listItems: IUser[];
  error: string | null;
  fullListLoaded: boolean;
  sortState: ISortState;
}
