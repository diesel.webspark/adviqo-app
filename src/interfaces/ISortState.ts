import { Sorting } from '../enums/Sorting';
import { Statuses } from '../enums/Statuses';
import { Languages } from '../enums/Languages';

export interface ISortState {
  sorting: Sorting;
  status: Statuses;
  languages: Languages[];
  page: number;
}
