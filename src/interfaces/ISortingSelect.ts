import { GetUserParams } from '../enums/GetUserParams';

export interface ISortingSelect {
  title: string;
  mode?: 'multiple';
  type: GetUserParams;
  cb: Function;
  options: Array<any>;
}
