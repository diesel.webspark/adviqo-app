import React from 'react';

export interface IContentContainer {
  children: React.ReactNode;
}
