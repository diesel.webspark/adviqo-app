
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

## Description

The project created with CRA app so i used typescript template. As filters changed i perform network request(all soring and filtering logic written at backend);

I did not use any store because all data that i have i need only at one component.

I did not include any css preprocessor because of css(with isolation) is enough for such small project.
